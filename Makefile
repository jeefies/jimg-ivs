CFLAGS = -g -I.
DEPS = `pkg-config opencv4 SDL2_ttf --cflags --libs`

main: jimg-ivs.hpp
	g++ -o main main.cpp $(DEPS) $(CFLAGS)

clean:
	rm -rf main out*
