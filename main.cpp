#include <iostream>
#include <vector>

#include "jimg-ivs.hpp"

using std::cerr;
using std::string;

int main(int argc, char *argv[]) {
	if (JIVS_Init() < 0) {
		return 1;
	}

	if (argc < 4) {
HELPER:
		cerr << "Usage: command [OPERATION] [OPTIONS] [INFILE] [OUTFILE]\n"
			<< "OPERATION can be e(encode) or d(decode)\n"
			<< "OPTIONS are used only in encode mode.\n"
			<< "	-sec [TEXT] give this program the text to encode. You can use it more than once.\n"
			<< "INFILE specific the file to input\n"
			<< "OUTFILE specific the file to output\n";
		return 0;
	}

	char * op = argv[1];
	if (op[0] == 'd') {
		// IN DECODE!!
		char *inf = argv[2], *outf = argv[3];
		JIVS_Decoder dec(inf);
		auto decodedImage = dec.decode();
		JIVS_save(decodedImage, outf);
	} else if (op[0] == 'e') {
		std::vector<char *> secs;

		bool isSec = false;
		int i;
		for (i = 2; i < argc; ++i) {
			if (isSec) {
				secs.push_back(argv[i]);
				isSec = false;
				continue;
			}

			if (argv[i][0] == '-') {
				cerr << "At argv " << i << " : " << argv[i] << '\n';
				if (argv[i][1] != 'e' || argv[i][2] != 'n') {
					cerr << "Invalid option...\n";
					return 1;
				}
				isSec = true;
				continue;
			}

			break;
		}

		char *inf = argv[i], *outf = argv[i + 1];
		JIVS_Encoder enc(inf, 20);
		for (char * text : secs)
			enc.addText(text);

		auto encodedImage = enc.encode();
		JIVS_save(encodedImage, outf);
	} else {
		cerr << "Unknown operation...\n";
		goto HELPER;
	}
	return 0;
}
