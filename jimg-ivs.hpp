#ifndef _JIMG_IVS_HPP_
#define _JIMG_IVS_HPP_

#include <iostream>
#include <vector>
#include <exception>
#include <string>
#include <cassert>

#include <opencv2/opencv.hpp>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#define RED 0
#define GREEN  1
#define BLUE 2

int JIVS_Init() {
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		std::cerr << "Could not init sdl: " << SDL_GetError() << '\n';
		return -1;
	}

	if (TTF_Init() != 0) {
		std::cerr << "TTF init error: " << TTF_GetError() << '\n';
		return -1;
	}

	return 0;
}

void showRendered(SDL_Surface * sur, int delay = 1) {
	SDL_Window *win;
	if ((win = SDL_CreateWindow("Hello World", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 
				sur->w, sur->h, SDL_WINDOW_SHOWN)) == nullptr) {
		std::cerr << "Could not create window: " << SDL_GetError() << '\n';
		return ;
	} 

	SDL_Surface *wsur = SDL_GetWindowSurface(win);
	SDL_BlitSurface(sur, NULL, wsur, NULL);
	SDL_UpdateWindowSurface(win);

	SDL_Delay(1000 * delay);
	
	SDL_DestroyWindow(win);
}

// Important when showing Chinese!
bool checkCompleteUTF8(const std::string &s) {
	char nBytes = 0;

	for (unsigned char c : s) {
		if (nBytes == 0 && c >= 0x80) {
			if (0xFC <= c && c <= 0xFD) nBytes = 6;
			else if (0xF8 <= c) nBytes = 5;
			else if (0xF0 <= c) nBytes = 4;
			else if (0xE0 <= c) nBytes = 3;
			else if (0xC0 <= c) nBytes = 2;
			else return false;
			--nBytes;
		} else if (nBytes) {
			if ((c & 0xC0) != 0x80) return false;
			--nBytes;
		}
	}

	if (nBytes > 0) return false;
	return true;
}

class Canvas {
private:
	bool _init;
	int w, h;
	int x, y, nextY; // current position.
	SDL_Surface *sur;
	TTF_Font *font;
public:
	Canvas() : _init(false) {}
	Canvas(int w, int h, int font_size = 14) : w(w), h(h), x(0), y(0), _init(true) {
		init(w, h, font_size);
	}

	~Canvas() {
		if (_init) {
			SDL_FreeSurface(sur);
			TTF_CloseFont(font);
		}
	}

	void init(int w, int h, int font_size = 14) {
		this->w = w, this->h = h;
		sur = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);
		if (sur == nullptr) {
			std::cerr << "Could not create surface: " << SDL_GetError() << '\n';
			throw -1;
		}

		font = TTF_OpenFont("font.ttf", font_size);
		if (font == nullptr) {
			std::cerr << "Font open failed: " << TTF_GetError() << '\n';
			throw -1;
		}
	}

	SDL_Surface * getSurface() {
		return sur;
	}

	int getWidth() { return w; }
	int getHeight() { return h; }

	void addText(const std::string &s) {
		if (s.length() == 0) return ;

		int sw = w + 2, sh = h + 2;
		TTF_SizeUTF8(font, s.c_str(), &sw, &sh);
		if (x + sw >= w) {
			// int len = s.size();
			// while (len > 0 && (!checkCompleteUTF8(s.substr(0, len)) || (~TTF_SizeUTF8(font, s.substr(0, len).c_str(), &sw, &sh) && x + sw >= w)))
			//	std::cerr << s.substr(0, len).c_str() << " got length " << sw << '\n',
			//	--len;
			int len = 0, lg = (int)log2(s.size()) + 1;
			for (int step = 1 << lg; step; step >>= 1) {
				if (len + step <= s.size()) {
					int clen = len + step;
					while (clen > len && !checkCompleteUTF8(s.substr(0, clen)))
						--clen;
					TTF_SizeUTF8(font, s.substr(0, clen).c_str(), &sw, &sh);
					if (x + sw < w) len = clen;
				}
			}

			TTF_SizeUTF8(font, s.substr(0, len).c_str(), &sw, &sh);
			if (x + sw >= w) {
				if (len == 0) {
					y = nextY + 5, x = 0;
					addText(s);
				} else {
					throw -1;
				}
				return ;
			}

			addText(s.substr(0, len));
			y = nextY + 5, x = 0;
			addText(s.substr(len));
			return ;
		}

		assert(checkCompleteUTF8(s));
		SDL_Surface *ss = TTF_RenderUTF8_Blended(font, s.c_str(), {0xFF, 0xFF, 0xFF, 0});
		SDL_Rect dst_rect = {x, y, ss->w, ss->h};
		SDL_BlitSurface(ss, NULL, sur, &dst_rect);
		x += ss->w + 5, nextY = std::max(nextY, y + ss->h);
	}
};

void invisibleWrite(SDL_Surface *sur, cv::Mat &mat) {
	SDL_LockSurface(sur);

	int w = sur->w, h = sur->h;
	Uint32 * pixels = (Uint32 *)sur->pixels;

	auto threeSumPixel = [](Uint32 cl) {
		return ((cl & 0xFF) + ((cl >> 8) & 0xFF) + ((cl >> 16) & 0xFF)) / 3;
	};

	auto discrete = [](unsigned char x) -> unsigned char { return (unsigned char)((round((double)x / 8)) * 8 - 1); };

	for (int i = 0; i < h; ++i) {
		int base = i * w;
		unsigned char* data = mat.ptr<unsigned char>(i);
		for (int j = 0; j < w; ++j) {
			if (threeSumPixel(pixels[base + j]) > 128) 
				data[j] = discrete(data[j]);
			else if (data[j] == discrete(data[j])) 
				data[j] = data[j] == 0 ? 1 : data[j] - 1;
		}
	}

	SDL_UnlockSurface(sur);
}

class JIVS_Encoder {
private:
	bool _open;
	Canvas cvs;
	cv::Mat image;
public:
	JIVS_Encoder() : _open(false) {}
	JIVS_Encoder(const std::string &fn, int font_size = 14) {
		open(fn, font_size);
	}

	void open(const std::string &fn, int font_size = 14) {
		_open = true;
		
		image = imread(fn, cv::IMREAD_COLOR);
		if (image.data == NULL) {
			std::cerr << "Open Failed!" << '\n';
			throw -1;
		}

		cvs.init(image.cols, image.rows, font_size);
	}

	// return -1 if no file open
	int addText(const std::string &istr) {
		std::string str = istr;
		std::replace_if(str.begin(), str.end(),
				[](char x) { return x == '\n' || x == '\t' || x == '\r'; }, ' ');
		std::cerr << "replace to " << str << '\n';
		if (!_open) return -1;
		cvs.addText(str);
		return 0;
	}

	cv::Mat encode(int channel_id = GREEN) {
		cv::Mat newImage;
		if (!_open) return newImage;
		std::vector<cv::Mat> channels;
		cv::split(image, channels);

		showRendered(cvs.getSurface(), 10);
		invisibleWrite(cvs.getSurface(), channels[channel_id]);
		cv::merge(channels, newImage);

		return newImage;
	}
};

SDL_Surface * invisibleRead(cv::Mat &mat) {
	int w = mat.cols, h = mat.rows;
	SDL_Surface * sur = SDL_CreateRGBSurface(0, w, h, 32, 0, 0, 0, 0);

	SDL_LockSurface(sur);
	Uint32 * pixels = (Uint32 *)sur->pixels;

	auto discrete = [](unsigned char x) -> unsigned char { return (unsigned char)((round((double)x / 8)) * 8 - 1); };

	for (int i = 0; i < h; ++i) {
		int base = i * w;
		unsigned char* data = mat.ptr<unsigned char>(i);

		for (int j = 0; j < w; ++j) {
			if (discrete(data[j]) == data[j]) pixels[base + j] = 0xFFFFFF;
		}
	}

	SDL_UnlockSurface(sur);
	return sur;
}

class JIVS_Decoder {
private:
	bool _open;
	cv::Mat image;
public:
	JIVS_Decoder() : _open(false) {}
	JIVS_Decoder(const std::string &fn) { open(fn); }

	void open(const std::string &fn) {
		_open = true;
		image = imread(fn, cv::IMREAD_COLOR);
		if (image.data == NULL) {
			std::cerr << "JIVS_Decoder open image failed" << '\n';
			throw -1;
		}
	}

	SDL_Surface* decode(int channel_id = GREEN) {
		if (!_open) 
			return nullptr;

		std::vector<cv::Mat> channels;
		cv::split(image, channels);

		SDL_Surface *sur = invisibleRead(channels[channel_id]);
		return sur;
	}
};

void JIVS_save(cv::Mat img, const std::string &fn) {
	cv::imwrite(fn, img);
}

void JIVS_save(SDL_Surface *sur, const std::string &fn) {
	SDL_SaveBMP(sur, fn.c_str());
}

#endif // _JIMG_IVS_HPP_
